<?php

declare(strict_types=1);

namespace UXF\Security\Event;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final readonly class LogoutUserEvent
{
    public function __construct(
        public Request $request,
        public Response $response,
    ) {
    }
}
