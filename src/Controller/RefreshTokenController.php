<?php

declare(strict_types=1);

namespace UXF\Security\Controller;

use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use UXF\Core\Attribute\FromBody;
use UXF\Core\Exception\BasicException;
use UXF\Core\Http\ResponseModifier;
use UXF\Security\Http\AuthResponse;
use UXF\Security\Http\RefreshTokenRequestBody;
use UXF\Security\Service\AuthService;

/**
 * @implements ResponseModifier<AuthResponse>
 */
final readonly class RefreshTokenController implements ResponseModifier
{
    public function __construct(private AuthService $authService)
    {
    }

    #[Route('/api/auth/refresh-token', name: 'auth_refresh_token', methods: 'POST')]
    public function __invoke(Request $request, #[FromBody] RefreshTokenRequestBody $body): AuthResponse
    {
        $token = $body->refreshToken;
        if ($token === null && $request->cookies->has(AuthService::RefreshTokenCookieName)) {
            $token = Uuid::fromString($request->cookies->get(AuthService::RefreshTokenCookieName, ''));
        }

        if ($token === null) {
            throw BasicException::unauthorized('Token not provided (in cookie or body)');
        }

        return $this->authService->refreshToken($token);
    }

    public static function modifyResponse(Response $response, mixed $data): void
    {
        $data->applyCookies($response);
    }
}
