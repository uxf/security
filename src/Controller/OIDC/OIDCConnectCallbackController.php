<?php

declare(strict_types=1);

namespace UXF\Security\Controller\OIDC;

use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Twig\Environment;
use UXF\Security\Exception\OIDCFlowException;
use UXF\Security\Service\OIDC\OIDC;
use UXF\Security\Service\OIDC\OIDCService;

final readonly class OIDCConnectCallbackController
{
    public function __construct(
        private OIDCService $service,
        private Security $security,
        private Environment $twig,
    ) {
    }

    #[Route('/api/auth/oidc/{provider}/connect-callback', name: 'auth_oidc_connect_callback', methods: 'GET')]
    #[Route('/api/auth/oidc/{provider}/connect-callback', name: 'auth_oidc_connect_callback_form', methods: 'POST')]
    public function __invoke(Request $request, OIDC $provider): Response
    {
        $user = $this->security->getUser();
        if ($user === null) {
            // fake redirect -> fix cookie issue
            if ($request->getMethod() === 'POST') {
                $idToken = $request->request->get('id_token');
                return new RedirectResponse("/api/auth/oidc/{$provider->value}/connect-callback?id_token=$idToken");
            }

            $html = $this->twig->render('@UXFSecurity/oidc/error.html.twig', [
                'message' => 'Login is required',
            ]);
            return new Response($html, 400);
        }

        try {
            $this->service->connect($provider, $request, $user);
        } catch (OIDCFlowException $e) {
            $html = $this->twig->render('@UXFSecurity/oidc/error.html.twig', [
                'message' => $e->getMessage(),
            ]);
            return new Response($html, 400);
        }

        $response = new RedirectResponse($request->cookies->get('oidc_redirect') ?? '/');
        $response->headers->removeCookie('oidc_redirect');
        return $response;
    }
}
