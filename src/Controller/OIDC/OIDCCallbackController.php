<?php

declare(strict_types=1);

namespace UXF\Security\Controller\OIDC;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Twig\Environment;
use UXF\Security\Exception\OIDCFlowException;
use UXF\Security\Service\OIDC\OIDC;
use UXF\Security\Service\OIDC\OIDCService;

final readonly class OIDCCallbackController
{
    public function __construct(
        private OIDCService $service,
        private Environment $twig,
    ) {
    }

    #[Route('/api/auth/oidc/{provider}/callback', name: 'auth_oidc_callback', methods: 'GET')]
    #[Route('/api/auth/oidc/{provider}/callback', name: 'auth_oidc_callback_form', methods: 'POST')]
    public function __invoke(Request $request, OIDC $provider): Response
    {
        try {
            $authResponse = $this->service->login($provider, $request);
        } catch (OIDCFlowException $e) {
            $html = $this->twig->render('@UXFSecurity/oidc/error.html.twig', [
                'message' => $e->getMessage(),
            ]);
            return new Response($html, 400);
        }

        $response = new RedirectResponse($request->cookies->get('oidc_redirect') ?? '/');
        $response->headers->removeCookie('oidc_redirect');
        $authResponse->applyCookies($response);
        return $response;
    }
}
