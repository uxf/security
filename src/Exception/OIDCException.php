<?php

declare(strict_types=1);

namespace UXF\Security\Exception;

use Psr\Log\LogLevel;
use RuntimeException;
use UXF\Core\Exception\CoreException;

final class OIDCException extends RuntimeException implements CoreException
{
    /**
     * @param LogLevel::* $level
     */
    public function __construct(
        string $message = 'OIDC Error',
        private readonly string $level = LogLevel::INFO,
    ) {
        parent::__construct($message);
    }

    public function getHttpStatusCode(): int
    {
        return 400;
    }

    public function getErrorCode(): string
    {
        return 'OIDC_ERROR';
    }

    /**
     * @inheritDoc
     */
    public function getValidationErrors(): ?array
    {
        return null;
    }

    public function getLevel(): string
    {
        return $this->level;
    }
}
