<?php

declare(strict_types=1);

namespace UXF\Security\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use UXF\Core\Type\DateTime;

#[ORM\Entity]
#[ORM\Table(schema: 'uxf_security')]
class RefreshToken
{
    public function __construct(
        #[ORM\Column(type: 'uuid'), ORM\Id]
        private UuidInterface $token,
        #[ORM\ManyToOne, ORM\JoinColumn(nullable: false)]
        private UserInterface $user,
        #[ORM\Column(type: DateTime::class)]
        private DateTime $expiration,
    ) {
    }

    public function getToken(): UuidInterface
    {
        return $this->token;
    }

    public function getUser(): UserInterface
    {
        return $this->user;
    }

    public function getExpiration(): DateTime
    {
        return $this->expiration;
    }
}
