<?php

declare(strict_types=1);

namespace UXF\Security;

use Symfony\Component\Config\Definition\Configurator\DefinitionConfigurator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\DependencyInjection\Loader\Configurator\ReferenceConfigurator;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;
use Symfony\Component\Security\Core\User\UserInterface;
use UXF\Security\Service\OIDC\OIDCService;
use UXF\Security\Service\OIDC\Provider\CodeProvider;
use UXF\Security\Service\OIDC\Provider\FormPostProvider;

final class UXFSecurityBundle extends AbstractBundle
{
    protected string $extensionAlias = 'uxf_security';

    public function configure(DefinitionConfigurator $definition): void
    {
        $definition->rootNode()
            ->children()
                ->scalarNode('user_class')->isRequired()->end()
                ->scalarNode('base_url')->isRequired()->end()
                ->scalarNode('public_key')->isRequired()->end()
                ->scalarNode('private_key')->isRequired()->end()
                ->scalarNode('access_token_lifetime')->defaultValue('P1D')->end()
                ->scalarNode('refresh_token_lifetime')->defaultValue('P1M')->end()
                ->scalarNode('refresh_token_cookie_path')->defaultValue(null)->end()
                ->scalarNode('cookie_name')->defaultValue('Authorization')->end()
                ->scalarNode('cookie_secured')->defaultValue(true)->end()
                ->scalarNode('cookie_http_only')->defaultValue(true)->end()
                ->arrayNode('oidc')
                    ->children()
                        ->arrayNode('apple')
                            ->children()
                                ->scalarNode('client_id')->isRequired()->end()
                            ->end()
                        ->end()
                        ->arrayNode('google')
                            ->children()
                                ->scalarNode('client_id')->isRequired()->end()
                            ->end()
                        ->end()
                        ->arrayNode('gitlab')
                            ->children()
                                ->scalarNode('client_id')->isRequired()->end()
                                ->scalarNode('client_secret')->isRequired()->end()
                            ->end()
                        ->end()
                        ->arrayNode('facebook')
                            ->children()
                                ->scalarNode('client_id')->isRequired()->end()
                                ->scalarNode('client_secret')->isRequired()->end()
                            ->end()
                        ->end()
                        ->arrayNode('microsoft')
                            ->children()
                                ->scalarNode('client_id')->isRequired()->end()
                            ->end()
                        ->end()
                        ->arrayNode('mojeid')
                            ->children()
                                ->scalarNode('client_id')->isRequired()->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();
    }

    /**
     * @param array<mixed> $config
     */
    public function loadExtension(array $config, ContainerConfigurator $container, ContainerBuilder $builder): void
    {
        $container->import(__DIR__ . '/../config/services.php');

        $parameters = $container->parameters();
        $parameters->set('uxf_security.public_key', $config['public_key']);
        $parameters->set('uxf_security.private_key', $config['private_key']);
        $parameters->set('uxf_security.access_token_lifetime', $config['access_token_lifetime']);
        $parameters->set('uxf_security.refresh_token_lifetime', $config['refresh_token_lifetime']);
        $parameters->set('uxf_security.refresh_token_cookie_path', $config['refresh_token_cookie_path']);
        $parameters->set('uxf_security.cookie_name', $config['cookie_name']);
        $parameters->set('uxf_security.cookie_secured', $config['cookie_secured']);
        $parameters->set('uxf_security.cookie_http_only', $config['cookie_http_only']);
        $parameters->set('uxf_security.base_url', $config['base_url']);

        $services = $container->services();

        // oicd
        $providers = [];

        $appleId = $config['oidc']['apple']['client_id'] ?? null;
        if ($appleId !== null) {
            $providers['apple'] = $services->set('uxf_security.oidc.apple', FormPostProvider::class)
                ->arg('$baseUrl', $config['base_url'])
                ->arg('$clientId', $appleId)
                ->autowire();
        }

        $facebookId = $config['oidc']['facebook']['client_id'] ?? null;
        if ($facebookId !== null) {
            $providers['facebook'] = $services->set('uxf_security.oidc.facebook', CodeProvider::class)
                ->arg('$baseUrl', $config['base_url'])
                ->arg('$clientId', $facebookId)
                ->arg('$clientSecret', $config['oidc']['facebook']['client_secret'])
                ->arg('$tokenUrl', 'https://graph.facebook.com/v13.0/oauth/access_token')
                ->autowire();
        }

        $gitlabId = $config['oidc']['gitlab']['client_id'] ?? null;
        if ($gitlabId !== null) {
            $providers['gitlab'] = $services->set('uxf_security.oidc.gitlab', CodeProvider::class)
                ->arg('$baseUrl', $config['base_url'])
                ->arg('$clientId', $gitlabId)
                ->arg('$clientSecret', $config['oidc']['gitlab']['client_secret'])
                ->arg('$tokenUrl', 'https://gitlab.com/oauth/token')
                ->autowire();
        }

        $googleId = $config['oidc']['google']['client_id'] ?? null;
        if ($googleId !== null) {
            $providers['google'] = $services->set('uxf_security.oidc.google', FormPostProvider::class)
                ->arg('$baseUrl', $config['base_url'])
                ->arg('$clientId', $googleId)
                ->autowire();
        }

        $microsoftId = $config['oidc']['microsoft']['client_id'] ?? null;
        if ($microsoftId !== null) {
            $providers['microsoft'] = $services->set('uxf_security.oidc.microsoft', FormPostProvider::class)
                ->arg('$baseUrl', $config['base_url'])
                ->arg('$clientId', $microsoftId)
                ->autowire();
        }

        $mojeId = $config['oidc']['mojeid']['client_id'] ?? null;
        if ($mojeId !== null) {
            $providers['mojeid'] = $services->set('uxf_security.oidc.mojeid', FormPostProvider::class)
                ->arg('$baseUrl', $config['base_url'])
                ->arg('$clientId', $mojeId)
                ->autowire();
        }

        $providers = array_combine(array_keys($providers), array_keys($providers));
        $services->set(OIDCService::class)
            ->arg('$providers', array_map(static fn (string $n) => new ReferenceConfigurator("uxf_security.oidc.$n"), $providers))
            ->autowire();
    }

    public function prependExtension(ContainerConfigurator $container, ContainerBuilder $builder): void
    {
        $config = array_merge(...$builder->getExtensionConfig('uxf_security'));

        // doctrine
        $container->extension('doctrine', [
            'orm' => [
                'resolve_target_entities' => [
                    UserInterface::class => $config['user_class'],
                ],
            ],
        ]);
    }
}
