<?php

declare(strict_types=1);

namespace UXF\Security\Service;

use Symfony\Component\Security\Core\User\UserInterface;
use UXF\Core\Exception\BasicException;

/**
 * @template T of UserInterface
 */
interface AuthUserProvider
{
    /**
     * @phpstan-return T|null
     */
    public function findByUsername(string $username): ?UserInterface;

    /**
     * @phpstan-param T $user
     * @throws BasicException
     */
    public function check(UserInterface $user, bool $refresh): void;
}
