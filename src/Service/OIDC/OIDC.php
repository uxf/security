<?php

declare(strict_types=1);

namespace UXF\Security\Service\OIDC;

use Nette\Utils\Json;
use UXF\Core\SystemProvider\Uuid;
use function Safe\file_get_contents;

enum OIDC: string
{
    case APPLE = 'apple';
    case FACEBOOK = 'facebook';
    case GITLAB = 'gitlab';
    case GOOGLE = 'google';
    case MICROSOFT = 'microsoft';
    case MOJEID = 'mojeid';

    public function getRedirectUrl(string $baseUrl, string $clientId, bool $connect): string
    {
        $responseType = match ($this) {
            self::APPLE, self::GOOGLE => 'code id_token',
            self::GITLAB, self::FACEBOOK => 'code',
            self::MICROSOFT, self::MOJEID => 'id_token',
        };

        $query = http_build_query([
            'scope' => 'openid email',
            'response_type' => $responseType,
            'redirect_uri' => $this->getAppRedirectUri($baseUrl, $connect),
            'client_id' => $clientId,
            'nonce' => Uuid::uuid4()->toString(),
            'response_mode' => 'form_post',
            // doesnt work with FB & gitlab :(
        ]);

        $configuration = $this->getConfiguration();
        $authorizationEndpoint = $configuration['authorization_endpoint'];

        return $authorizationEndpoint . '?' . $query;
    }

    /**
     * @return array<string, mixed>
     */
    public function getConfiguration(): array
    {
        $providerUrl = match ($this) {
            self::APPLE => 'https://appleid.apple.com',
            self::FACEBOOK => 'https://facebook.com',
            self::GITLAB => 'https://gitlab.com',
            self::GOOGLE => 'https://accounts.google.com',
            self::MICROSOFT => 'https://login.microsoftonline.com/common/v2.0',
            self::MOJEID => 'https://mojeid.cz',
        };

        return Json::decode(file_get_contents($providerUrl . '/.well-known/openid-configuration'), forceArrays: true);
    }

    public function getAppRedirectUri(string $baseUrl, bool $connect): string
    {
        $url = $connect ? 'connect-callback' : 'callback';
        return $baseUrl . "/api/auth/oidc/{$this->value}/{$url}";
    }
}
