<?php

declare(strict_types=1);

namespace UXF\Security\Service\OIDC;

use Symfony\Component\Security\Core\User\UserInterface;

final class NewUserEvent
{
    public function __construct(
        public readonly OIDCInfo $oidcInfo,
        public ?UserInterface $user = null,
    ) {
    }
}
