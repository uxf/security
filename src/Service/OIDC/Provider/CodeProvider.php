<?php

declare(strict_types=1);

namespace UXF\Security\Service\OIDC\Provider;

use Nette\Utils\Json;
use Symfony\Component\HttpFoundation\Request;
use UXF\Security\Service\OIDC\OIDC;
use UXF\Security\Service\OIDC\OIDCInfo;
use function Safe\curl_exec;
use function Safe\curl_init;
use function Safe\curl_setopt;

final readonly class CodeProvider implements OIDCProvider
{
    public function __construct(
        private string $baseUrl,
        private string $clientId,
        private string $clientSecret,
        private string $tokenUrl,
    ) {
    }

    public function getRedirectUrl(OIDC $provider, bool $connect): string
    {
        return $provider->getRedirectUrl($this->baseUrl, $this->clientId, $connect);
    }

    public function authenticate(OIDC $provider, Request $request, bool $connect): OIDCInfo
    {
        $code = $request->query->get('code');
        $redirectUri = $provider->getAppRedirectUri($this->baseUrl, $connect);

        $form = [
            'code' => $code,
            'client_id' => $this->clientId,
            'client_secret' => $this->clientSecret,
            'redirect_uri' => $redirectUri,
            'grant_type' => 'authorization_code',
        ];

        $ch = curl_init($this->tokenUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $form);
        $response = curl_exec($ch);
        assert(is_string($response));
        curl_close($ch);

        $idToken = Json::decode($response, forceArrays: true)['id_token'];

        return OIDCInfo::createFromIdToken($provider, $idToken);
    }
}
