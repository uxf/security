<?php

declare(strict_types=1);

namespace UXF\Security\Service\OIDC\Provider;

use Symfony\Component\HttpFoundation\Request;
use UXF\Security\Service\OIDC\OIDC;
use UXF\Security\Service\OIDC\OIDCInfo;

final readonly class FormPostProvider implements OIDCProvider
{
    public function __construct(
        private string $baseUrl,
        private string $clientId,
    ) {
    }

    public function getRedirectUrl(OIDC $provider, bool $connect): string
    {
        return $provider->getRedirectUrl($this->baseUrl, $this->clientId, $connect);
    }

    public function authenticate(OIDC $provider, Request $request, bool $connect): OIDCInfo
    {
        $idToken = $request->request->get('id_token') ?? $request->get('id_token'); // fallback to query param
        assert(is_string($idToken));

        return OIDCInfo::createFromIdToken($provider, $idToken);
    }
}
