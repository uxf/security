<?php

declare(strict_types=1);

namespace UXF\Security\Service\OIDC\Provider;

use Symfony\Component\HttpFoundation\Request;
use UXF\Security\Service\OIDC\OIDC;
use UXF\Security\Service\OIDC\OIDCInfo;

interface OIDCProvider
{
    public function getRedirectUrl(OIDC $provider, bool $connect): string;

    public function authenticate(OIDC $provider, Request $request, bool $connect): OIDCInfo;
}
