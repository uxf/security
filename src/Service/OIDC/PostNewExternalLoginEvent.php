<?php

declare(strict_types=1);

namespace UXF\Security\Service\OIDC;

use UXF\Security\Entity\ExternalLogin;

final readonly class PostNewExternalLoginEvent
{
    public function __construct(
        public ExternalLogin $externalLogin,
        public OIDCInfo $oidcInfo,
    ) {
    }
}
