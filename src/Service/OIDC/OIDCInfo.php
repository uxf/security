<?php

declare(strict_types=1);

namespace UXF\Security\Service\OIDC;

use Firebase\JWT\JWK;
use Firebase\JWT\JWT;
use Nette\Utils\Json;
use UXF\Core\Type\Email;
use function Safe\file_get_contents;

final readonly class OIDCInfo
{
    /**
     * @param array<string, mixed> $claims
     */
    public function __construct(
        public OIDC $provider,
        public string $sub,
        public Email $email,
        public ?bool $emailVerified,
        public array $claims,
    ) {
    }

    public static function createFromIdToken(OIDC $provider, string $idToken): OIDCInfo
    {
        $configuration = $provider->getConfiguration();

        $jwks = Json::decode(file_get_contents($configuration['jwks_uri']), forceArrays: true);

        // fix alg
        foreach ($jwks['keys'] as $i => $key) {
            $jwks['keys'][$i]['alg'] ??= 'RS256';
        }

        $userData = (array) JWT::decode($idToken, JWK::parseKeySet($jwks));

        $verified = $userData['email_verified'] ?? null;
        if (is_string($verified)) {
            $verified = $verified === 'true';
        }

        return new self(
            provider: $provider,
            sub: $userData['sub'],
            email: Email::of($userData['email']),
            emailVerified: $verified,
            claims: $userData,
        );
    }
}
