<?php

declare(strict_types=1);

namespace UXF\Security\Service\Jwt;

use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Throwable;
use UXF\Core\SystemProvider\Clock;
use UXF\Security\Exception\JwtDecodeException;

final readonly class JwtDecoder
{
    public function __construct(private string $publicKey)
    {
    }

    /**
     * @return array<string, mixed>
     */
    public function decode(string $token): array
    {
        try {
            JWT::$timestamp = Clock::now()->getTimestamp();
            return (array) JWT::decode($token, new Key($this->publicKey, 'RS256'));
        } catch (Throwable $exception) {
            throw JwtDecodeException::createFromException($exception);
        }
    }
}
