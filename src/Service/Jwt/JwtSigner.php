<?php

declare(strict_types=1);

namespace UXF\Security\Service\Jwt;

use Firebase\JWT\JWT;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use UXF\Security\Service\Jwk\JwkProvider;

final readonly class JwtSigner
{
    public function __construct(
        private string $privateKey,
        private string $baseUrl,
        private UrlGeneratorInterface $urlGenerator,
    ) {
    }

    /**
     * @param array{uid: int, exp: int, iat: int} $payload
     */
    public function sign(array $payload): string
    {
        $payload['iss'] = $this->baseUrl . $this->urlGenerator->generate('auth_keys');

        return JWT::encode($payload, $this->privateKey, 'RS256', JwkProvider::KEY_ID);
    }
}
