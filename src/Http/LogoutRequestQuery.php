<?php

declare(strict_types=1);

namespace UXF\Security\Http;

final readonly class LogoutRequestQuery
{
    public function __construct(public ?string $redirect = null)
    {
    }
}
