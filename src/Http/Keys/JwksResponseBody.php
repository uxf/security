<?php

declare(strict_types=1);

namespace UXF\Security\Http\Keys;

final readonly class JwksResponseBody
{
    /**
     * @param array<JwkResponseBody> $keys
     */
    public function __construct(public array $keys)
    {
    }
}
