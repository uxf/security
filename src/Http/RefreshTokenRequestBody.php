<?php

declare(strict_types=1);

namespace UXF\Security\Http;

use Ramsey\Uuid\UuidInterface;

final readonly class RefreshTokenRequestBody
{
    public function __construct(
        public ?UuidInterface $refreshToken = null,
    ) {
    }
}
