<?php

declare(strict_types=1);

namespace UXF\Security\Http;

use Ramsey\Uuid\UuidInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;

final readonly class AuthResponse
{
    /**
     * @param Cookie[] $cookies
     */
    public function __construct(
        public string $accessToken,
        public UuidInterface $refreshToken,
        private array $cookies, // internal
    ) {
    }

    /**
     * @deprecated
     */
    public function getCookie(): Cookie
    {
        trigger_error('Please use AuthResponse::applyCookies()', E_USER_DEPRECATED);
        return $this->cookies[0];
    }

    public function applyCookies(Response $response): void
    {
        foreach ($this->cookies as $cookie) {
            $response->headers->setCookie($cookie);
        }
    }
}
