<?php

declare(strict_types=1);

namespace UXF\SecurityTests\Project;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use UXF\Security\Service\AuthUserProvider;
use UXF\SecurityTests\Project\Entity\TestUser;

/**
 * @implements AuthUserProvider<TestUser>
 */
final readonly class TestAuthUserProvider implements AuthUserProvider
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function findByUsername(string $username): ?TestUser
    {
        return $this->entityManager->getRepository(TestUser::class)->findOneBy([
            'username' => $username,
        ]);
    }

    public function check(UserInterface $user, bool $refresh): void
    {
    }
}
