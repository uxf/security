<?php

declare(strict_types=1);

namespace UXF\SecurityTests\Project;

use Symfony\Component\EventDispatcher\Attribute\AsEventListener;
use UXF\Security\Event\LogoutUserEvent;

#[AsEventListener(LogoutUserEvent::class)]
final readonly class LogoutUserListener
{
    public function __invoke(LogoutUserEvent $event): void
    {
        $event->response->headers->add([
            'X-Logout' => '1',
        ]);
    }
}
