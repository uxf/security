<?php

declare(strict_types=1);

namespace UXF\SecurityTests\Project\Controller;

use UXF\Core\Attribute\Security;
use UXF\SecurityTests\Project\UserRole;

final readonly class FakeSecurityController
{
    #[Security(UserRole::ROLE_ROOT)]
    public function __invoke(): void
    {
    }
}
