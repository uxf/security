<?php

declare(strict_types=1);

namespace UXF\SecurityTests\Story;

use Firebase\JWT\JWK;
use Firebase\JWT\JWT;
use UXF\Core\SystemProvider\Uuid;
use UXF\Core\Test\WebTestCase;

class SmokeStoryTest extends WebTestCase
{
    public function testLogin(): void
    {
        Uuid::$seq = 1; // required for CI test (no idea why)

        $client = self::createClient();

        $client->get('/public');
        self::assertResponseIsSuccessful();

        $client->get('/private');
        self::assertResponseStatusCodeSame(401);

        // attribute
        $client->get('/security');
        self::assertResponseStatusCodeSame(401);

        // login
        $client->post('/api/auth/login', [
            'username' => 'user',
            'password' => 'pass',
        ]);
        self::assertResponseIsSuccessful();
        $this->assertSnapshot($client->getResponseData());

        $refreshToken = $client->getResponseData()['refreshToken'];

        $cookies = $client->getResponse()->headers->getCookies();

        self::assertSame('Cookie-Name', $cookies[0]->getName());
        self::assertSame('eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IkEifQ.eyJ1aWQiOjEsImV4cCI6MTk1NjUyNDQwMCwiaWF0IjoxNjQwOTkxNjAwLCJpc3MiOiJodHRwczovL3V4Zi5jei9hcGkvYXV0aC9rZXlzIn0.FbOXlrM8XVEeERfIFcqf7alwAXqlvUCePvQiRm2FE97oedw10ZG9J_QQwVEogvory0YszWdA0pUnl3us09tr-CsyanpFHtAu0o2YrghIa_4GBMpAJ9xgqz3YOYr1bvev7ADhZTBLGkHYe9u1UArLGDMSVhmtiTszJaDqMOzqPYByxE1yc7NYQPo2T6zuBxDRNI-WaIsI999m6cJ7B7Y-qgFN34VZ42KJf5mk9Yify6-mYb8OfwtYgep4GNhC90G97V2sjIslt1CW3-d7LkhlVDZHj6JGoQOnfizr-cethZ9cwJ2mysWAcGwJpoO-IVpAxXCGxMZxdTlowcCR1u7GOA', $cookies[0]->getValue());
        self::assertFalse($cookies[0]->isSecure());
        self::assertFalse($cookies[0]->isHttpOnly());

        self::assertSame('Cookie-Name-Expire', $cookies[1]->getName());
        self::assertSame('1956524400', $cookies[1]->getValue());
        self::assertFalse($cookies[1]->isSecure());
        self::assertFalse($cookies[1]->isHttpOnly());

        self::assertSame('uxf_auth_refresh_token', $cookies[2]->getName());
        self::assertSame('00000000-0000-4000-0000-000000000001', $cookies[2]->getValue());
        self::assertSame('/api/auth/refresh-token', $cookies[2]->getPath());
        self::assertFalse($cookies[2]->isSecure());
        self::assertTrue($cookies[2]->isHttpOnly());

        // refresh token by cookie
        $client->post('/api/auth/refresh-token', []);
        self::assertResponseIsSuccessful();

        // refresh token by body
        $client->post('/api/auth/refresh-token', [
            'refreshToken' => $refreshToken,
        ]);
        self::assertResponseIsSuccessful();
        $this->assertSnapshot($client->getResponseData());

        $client->get('/public');
        self::assertResponseIsSuccessful();

        $client->get('/private');
        self::assertResponseIsSuccessful();

        $client->get('/private/root');
        self::assertResponseStatusCodeSame(403);

        // attribute
        $client->get('/security');
        self::assertResponseStatusCodeSame(403);

        $client->get('/api/auth/logout');
        self::assertResponseStatusCodeSame(200);
        self::assertResponseHeaderSame('X-Logout', '1'); // check event

        $client->get('/api/auth/logout?redirect=/');
        self::assertResponseStatusCodeSame(302);
        self::assertResponseRedirects('/');
    }

    public function testKeys(): void
    {
        $client = self::createClient();

        // test public keys
        $client->get('/api/auth/keys');
        self::assertResponseIsSuccessful();
        $this->assertSnapshot($client->getResponseData());
        $jwks = $client->getResponseData();

        // login
        $client->post('/api/auth/login', [
            'username' => 'user',
            'password' => 'pass',
        ]);
        $accessToken = $client->getResponseData()['accessToken'];

        JWT::decode($accessToken, JWK::parseKeySet($jwks));
    }
}
