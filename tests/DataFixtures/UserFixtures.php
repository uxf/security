<?php

declare(strict_types=1);

namespace UXF\SecurityTests\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use UXF\SecurityTests\Project\Entity\TestUser;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $user = new TestUser(1, 'user');
        $manager->persist($user);
        $manager->flush();
    }
}
