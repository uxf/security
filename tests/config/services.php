<?php

declare(strict_types=1);

use Ramsey\Uuid\Doctrine\UuidType;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use UXF\Core\Test\Client;
use UXF\Security\Service\Authenticator;
use UXF\Security\Service\AuthUserProvider;
use UXF\SecurityTests\DataFixtures\UserFixtures;
use UXF\SecurityTests\Project\Entity\TestUser;
use UXF\SecurityTests\Project\LogoutUserListener;
use UXF\SecurityTests\Project\TestAuthUserProvider;

return static function (ContainerConfigurator $containerConfigurator): void {
    $services = $containerConfigurator->services();

    $services->defaults()
        ->autowire()
        ->autoconfigure()
        ->public();

    $services->set(Client::class);
    $services->alias('test.client', Client::class);

    $services->set(UserFixtures::class);
    $services->set(LogoutUserListener::class);

    $containerConfigurator->extension('framework', [
        'secret' => 'test-secret',
        'test' => true,
        'validation' => [
            'email_validation_mode' => 'html5',
        ],
        'http_method_override' => false,
    ]);

    $containerConfigurator->extension('security', [
        'password_hashers' => [
            TestUser::class => 'plaintext',
        ],
        'providers' => [
            'provider' => [
                'entity' => [
                    'class' => TestUser::class,
                    'property' => 'id',
                ],
            ],
        ],
        'firewalls' => [
            'secured' => [
                'stateless' => true,
                'custom_authenticators' => [Authenticator::class],
            ],
        ],
        'access_control' => [
            [
                'path' => '^/public',
                'roles' => 'PUBLIC_ACCESS',
            ],
            [
                'path' => '^/private/root',
                'roles' => 'ROLE_ROOT',
            ],
            [
                'path' => '^/private',
                'roles' => 'ROLE_USER',
            ],
        ],
    ]);

    $containerConfigurator->extension('doctrine', [
        'dbal' => [
            'url' => 'sqlite:///%kernel.project_dir%/var/db.sqlite',
            'types' => [
                'uuid' => UuidType::class,
            ],
        ],
        'orm' => [
            'naming_strategy' => 'doctrine.orm.naming_strategy.underscore_number_aware',
            'auto_mapping' => true,
            'report_fields_where_declared' => true,
            'enable_lazy_ghost_objects' => true,
            'mappings' => [
                'UXFSecurityTest' => [
                    'type' => 'attribute',
                    'dir' => __DIR__ . '/../Project/Entity',
                    'prefix' => 'UXF\SecurityTests\Project\Entity',
                ],
            ],
        ],
    ]);

    $containerConfigurator->extension('uxf_security', [
        'user_class' => TestUser::class,
        'base_url' => 'https://uxf.cz',
        'public_key' => '%env(AUTH_PUBLIC_KEY)%',
        'private_key' => '%env(AUTH_PRIVATE_KEY)%',
        'access_token_lifetime' => 'P10Y',
        'refresh_token_lifetime' => 'P20Y',
        'refresh_token_cookie_path' => '/api/auth/refresh-token',
        'cookie_name' => 'Cookie-Name',
        'cookie_secured' => false,
        'cookie_http_only' => false,
        'oidc' => [
            'apple' => [
                'client_id' => 'APPLE',
            ],
            'facebook' => [
                'client_id' => 'FACEBOOK_ID',
                'client_secret' => 'FACEBOOK_SECRET',
            ],
            'gitlab' => [
                'client_id' => 'GITLAB_ID',
                'client_secret' => 'GITLAB_SECRET',
            ],
            'google' => [
                'client_id' => 'GOOGLE',
            ],
            'microsoft' => [
                'client_id' => 'MICROSOFT',
            ],
            'mojeid' => [
                'client_id' => 'MOJEID',
            ],
        ],
    ]);

    $services->set(AuthUserProvider::class, TestAuthUserProvider::class);
};
